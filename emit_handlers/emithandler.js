'use strict';

let events = require('events');
let emitter = new events.EventEmitter();
let request_count_checker = require('./../models/server/middlewares/request_count_checker');
let cache_api = require('./../models/cache/cache_api');
let get = require('./../models/server/request/handlers/get');
let post = require('./../models/server/request/handlers/post');
let http_get = require('./../models/httpclient/get');
let http_post = require('./../models/httpclient/post');
let request_type_checker = require('./../models/server/middlewares/request_type_checker');

emitter.on('handler__cache_api', (req, res, respond)=>{
    console.log('handler_cache_api emit handled');
    cache_api(req, res, respond);
});

emitter.on('handler__request_count', (req, res, respond)=>{
    console.log('handler_request_count_checker emit handled');
    request_count_checker(req, res, respond);
});

emitter.on('request_count__type_checker', (req, res, respond)=>{
    console.log('request_count__type_checker emit handled');
    request_type_checker(req, res, respond);
});

emitter.on('type_checker__get', (req, res, respond)=>{
    console.log('request_count__type_checker emit handled');
    get(req, res, respond);
});

emitter.on('type_checker__post', (req, res, respond)=>{
    console.log('request_count__type_checker emit handled');
    post(req, res, respond);
});

emitter.on('get__http_get', (req, res, respond)=>{
    console.log('request_count__type_checker emit handled');
    http_get(req, res, respond);
});

emitter.on('post__http_post', (url, headers, input, req, res, respond)=>{
    console.log('request_count__type_checker emit handled');
    http_post(url, headers, input, req, res, respond);
});

module.exports = emitter;
