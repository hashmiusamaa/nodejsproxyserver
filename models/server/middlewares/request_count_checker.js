'use strict';

let ip_browser_dict = {};
let ip_browser_timestamp_dict = {};
// taken from TAs solution
let getParsedCookies = function (request){
    let cookies = {};
    let requestCookies = request.headers.cookie;
    if (requestCookies) {
        requestCookies.split(';').forEach(function(cookie){
            let chunk = cookie.split('=');
            cookies[chunk.shift().trim()] = decodeURI(chunk.join('='));
        });
    }
    return cookies;
};

let count_checker = (req, res, respond) => {
    let emitter = require('./../../../emit_handlers/emithandler');
    let req_cookies = getParsedCookies(req);
    res.cook = {};
    console.log(JSON.stringify(req_cookies));
    if(req_cookies.visitcount === undefined || req_cookies.visittime === undefined){
        console.log('first entry');
        res.cook.count = 'visitcount=1';
        res.cook.time = 'visittime='+new Date().getTime();
        emitter.emit("request_count__type_checker", req, res, respond);
    }else{
        if(req_cookies.visitcount < 10){
            console.log('count found');
            let count = parseInt(req_cookies.visitcount);
            count++;
            res.cook.count = 'visitcount='+count;
            res.cook.time = 'visittime='+req_cookies.visittime;
            emitter.emit('request_count__type_checker', req, res, respond);
        }else{
            if(!(req_cookies.visitcount < 10)){
                if(Math.abs(new Date().getTime() - parseInt(req_cookies.visittime)) > 10*60*1000){
                    res.cook.count = 'visitcount=1';
                    res.cook.time = 'visittime='+new Date().getTime();
                    emitter.emit('request_count__type_checker', req, res, respond);
                }else{
                    console.log('I AM THE TROUBLE MAKER');
                    let resp = {
                        statusCode:200,
                        statusMessage:'too many requests',
                        response:'too many requests',
                        header:{'Content-Type':'text/html'}
                    };
                    respond(req, res, resp);
                }
            }
        }
    }
};

module.exports = count_checker;