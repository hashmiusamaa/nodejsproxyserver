'use strict';

let cache = require('../../../../models/cache/lrucache');
// step 2: processing the query string here.
let get_handler = function(req, res, respond){
    let emitter = require('./../../../../emit_handlers/emithandler');
    // here first check if available in cache
    let cached_data = cache.get(JSON.stringify({'method':'get','url':req.url}));
    if(cached_data !== undefined){
        // if not call http get method
        respond(req, res, cached_data.resp);
    }else{
        emitter.emit('get__http_get', req, res, respond);
    }
};

module.exports = get_handler;