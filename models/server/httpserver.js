'use strict';

const http = require('http');
let server_options = require('../../config/components/server_options');
let request_handler = require('./request/handler');
let cache = require('../../models/cache/lrucache');

let server = http.createServer(request_handler);

server.listen(server_options.port, function (err) {
    if(err){
        console.log("Error starting server.");
    }else{
        console.log("Server is now listening on port", server_options.port);
        setInterval(()=>{
            cache.rforEach(function(value,key,cache){
                cache.get(key);
            });
        }, 1000*server_options.freshness);
    }
});

module.exports = server;