'use strict';
// @Rachana: write a method that will get data from client
// and return an object containing head and data
let server_options = require("../../config/components/server_options");
let cache = require('../cache/lrucache');
let http = require('http');

let http_get = (req, res, respond) => {
    console.log('http_get');
    http.get({
        host: server_options.server,
        port: server_options.server_port,
        path: req.url
    }, (response) =>{
        //response.pipe(res);
        const chunks = [];

        response.on("data", function (chunk) {
            chunks.push(chunk);
        });

        response.on("end", function () {
            if(response.headers.location !== undefined){
                console.log("LOCATION:  "+response.headers.location);
                if(response.headers.location.substring(0,5).toLowerCase() === 'https'){
                    console.log("HTTPS Redirect");
                    respond(req, res, {
                        statusCode: 302, statusMessage:'HTTPS Redirect',
                        response: 'HTTPS Redirect', headers:{'content-type':'text/html'}});
                }else{
                    console.log('HTTP REDIRECT');
                    normal_response(req, res, respond, response, chunks);
                }
            }else {
                console.log('Normal Exec');
                normal_response(req, res, respond, response, chunks);
            }
        });
    }).on('error', function (error) {
        console.log(error);
        let resp = {
            statusCode: 404,
            statusMessage: error,
            response: ''+error,
            headers: {'content-type':'text/html'}
        };
        respond(req, res, resp);
    });
};

let normal_response = function(req, res, respond, response, chunks){
    let resp = {
        statusCode: response.statusCode,
        statusMessage: {
            'Content-Type': response.headers['content-type']
            //, 'filename': response.headers['filename']
        },
        headers: response.headers,
        response: Buffer.concat(chunks)
    };
    cache.set(JSON.stringify({'method': 'get', 'url': req.url}), {resp: resp});
    respond(req, res, resp);
};

module.exports = http_get;

